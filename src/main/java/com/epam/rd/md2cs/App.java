package com.epam.rd.md2cs;

import java.util.Arrays;

public class App {
    public static void main(String...args) {
        Arrays.stream(args).forEach(System.out::println);
    }
}
